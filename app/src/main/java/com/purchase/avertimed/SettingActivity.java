package com.purchase.avertimed;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;

import com.purchase.avertimed.API.UserSession;

public class SettingActivity extends AppCompatActivity {

     private SwitchCompat swOnOff;
    private UserSession session;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        session = new UserSession(getApplicationContext());

        swOnOff = findViewById(R.id.swOnOff);

        /*if(session.isNotification()){
            swOnOff.setChecked(true);
        }else {
            swOnOff.setChecked(false);
        }*/
        swOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                session.setNotification(isChecked);
              //  swOnOff.setChecked(true);
            }
        });
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.ln_changepassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(SettingActivity.this,ChangePassword.class);
                startActivity(i);
            }
        });

        findViewById(R.id.ln_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(SettingActivity.this,EditProfile.class);
                startActivity(i);
            }
        });

        findViewById(R.id.ln_shipping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(SettingActivity.this,ShippingAddress.class);
                startActivity(i);
            }
        });

    }
}