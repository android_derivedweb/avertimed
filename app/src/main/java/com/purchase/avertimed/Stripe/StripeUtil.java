package com.purchase.avertimed.Stripe;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;

import com.stripe.android.PaymentAuthConfig;
import com.stripe.android.PaymentSessionConfig;
import com.stripe.android.Stripe;
import com.stripe.android.model.ShippingInformation;
import com.stripe.android.stripe3ds2.init.ui.StripeButtonCustomization;
import com.stripe.android.stripe3ds2.init.ui.StripeLabelCustomization;
import com.stripe.android.stripe3ds2.init.ui.StripeTextBoxCustomization;
import com.stripe.android.stripe3ds2.init.ui.StripeToolbarCustomization;

public class StripeUtil {
    public static final String STRIPE_PUBLISHER_KEY = "pk_test_51InTUfLsL06gKf2abOFVTdoaeMPH65HllvWJIJth1IUjoq0VTso70tphVOuTESPecSZH8vmlmposRCjylOW2oZQk00ZppN8phb";
//    public static final String STRIPE_PUBLISHER_KEY = "pk_test_qhlpDXqdhSuAVDStY3AfkjVI";

    private static PaymentSessionHandler paymentSessionHandler;

    public static PaymentSessionHandler getPaymentSessionHandler(Activity activity) {
        if (paymentSessionHandler == null)
            paymentSessionHandler = new PaymentSessionHandler(activity);
        paymentSessionHandler.setActivity(activity);
        return paymentSessionHandler;
    }

    public static PaymentSessionConfig defaultPaymentSessionConfig(Activity activity) {
     //   UserData user = new Gson().fromJson(new PrefManager(activity).getUserData(), UserData.class);
        return new PaymentSessionConfig.Builder()
                .setShippingInfoRequired(false)
                .setShippingMethodsRequired(false)
                .setShouldShowGooglePay(false)
                .setShouldPrefetchCustomer(true)
                .setPrepopulatedShippingInfo(new ShippingInformation(null, null, null))
                .build();
    }


    public static Stripe getStripe(Activity activity, String stripeAccountKey) {
        if (TextUtil.isNullOrEmpty(stripeAccountKey)) {
            return new Stripe(activity, STRIPE_PUBLISHER_KEY);
        } else {
            return new Stripe(activity, STRIPE_PUBLISHER_KEY, stripeAccountKey);
        }
    }


    private static final String COLOR_ACCENT = "#000000";
    private static final String COLOR_PRIMARY = "#000000";
    private static final String COLOR_PRIMARY_DARK = "#000000";
    private static final String COLOR_WHITE = "#FFFFFF";

    private static final String FONT_REGULAR = "sf_regular.otf";
    private static final String FONT_SEMI_BOLD = "sm_semibold.otf";
    private static final String FONT_BOLD = "sf_bold.otf";

    @SuppressLint("ResourceType")
    public static void applyTheme(Context context) {
        Resources resources = context.getResources();

        StripeToolbarCustomization stripeToolbarCustomization = new StripeToolbarCustomization();
        stripeToolbarCustomization.setBackgroundColor(COLOR_PRIMARY);
        stripeToolbarCustomization.setStatusBarColor(COLOR_PRIMARY_DARK);
        stripeToolbarCustomization.setTextColor(COLOR_WHITE);
        stripeToolbarCustomization.setTextFontName(FONT_BOLD);

        StripeLabelCustomization stripeLabelCustomization = new StripeLabelCustomization();
        stripeLabelCustomization.setHeadingTextFontName(FONT_SEMI_BOLD);
        stripeLabelCustomization.setHeadingTextColor(COLOR_ACCENT);
        stripeLabelCustomization.setTextColor(COLOR_ACCENT);
        stripeLabelCustomization.setTextFontName(FONT_REGULAR);

        StripeTextBoxCustomization stripeTextBoxCustomization = new StripeTextBoxCustomization();
        stripeTextBoxCustomization.setTextFontName(FONT_SEMI_BOLD);
        stripeTextBoxCustomization.setBorderColor(COLOR_ACCENT);
        stripeTextBoxCustomization.setTextColor(COLOR_ACCENT);

        StripeButtonCustomization stripeButtonCustomization = new StripeButtonCustomization();
        stripeButtonCustomization.setCornerRadius(50);
        stripeButtonCustomization.setTextColor(COLOR_WHITE);
        stripeButtonCustomization.setTextFontName(FONT_SEMI_BOLD);
        stripeButtonCustomization.setBackgroundColor(COLOR_ACCENT);

        StripeButtonCustomization stripeButtonCustomizationCancel = new StripeButtonCustomization();
        stripeButtonCustomizationCancel.setTextColor(COLOR_ACCENT);
        stripeButtonCustomizationCancel.setTextFontName(FONT_SEMI_BOLD);

        StripeButtonCustomization stripeButtonCustomizationSelect = new StripeButtonCustomization();
        stripeButtonCustomizationSelect.setTextColor(COLOR_WHITE);
        stripeButtonCustomizationSelect.setBackgroundColor(COLOR_ACCENT);
        stripeButtonCustomizationSelect.setTextFontName(FONT_SEMI_BOLD);

        final PaymentAuthConfig.Stripe3ds2UiCustomization uiCustomization =
                new PaymentAuthConfig.Stripe3ds2UiCustomization.Builder()
                        .setAccentColor(COLOR_ACCENT)
                        .setTextBoxCustomization(new PaymentAuthConfig.Stripe3ds2TextBoxCustomization(stripeTextBoxCustomization))
                        .setToolbarCustomization(new PaymentAuthConfig.Stripe3ds2ToolbarCustomization(stripeToolbarCustomization))
                        .setLabelCustomization(new PaymentAuthConfig.Stripe3ds2LabelCustomization(stripeLabelCustomization))
                        .setButtonCustomization(new PaymentAuthConfig.Stripe3ds2ButtonCustomization(stripeButtonCustomization), PaymentAuthConfig.Stripe3ds2UiCustomization.ButtonType.CONTINUE)
                        .setButtonCustomization(new PaymentAuthConfig.Stripe3ds2ButtonCustomization(stripeButtonCustomization), PaymentAuthConfig.Stripe3ds2UiCustomization.ButtonType.NEXT)
                        .setButtonCustomization(new PaymentAuthConfig.Stripe3ds2ButtonCustomization(stripeButtonCustomization), PaymentAuthConfig.Stripe3ds2UiCustomization.ButtonType.SUBMIT)
                        .setButtonCustomization(new PaymentAuthConfig.Stripe3ds2ButtonCustomization(stripeButtonCustomization), PaymentAuthConfig.Stripe3ds2UiCustomization.ButtonType.RESEND)
                        .setButtonCustomization(new PaymentAuthConfig.Stripe3ds2ButtonCustomization(stripeButtonCustomizationCancel), PaymentAuthConfig.Stripe3ds2UiCustomization.ButtonType.CANCEL)
                        .setButtonCustomization(new PaymentAuthConfig.Stripe3ds2ButtonCustomization(stripeButtonCustomizationSelect), PaymentAuthConfig.Stripe3ds2UiCustomization.ButtonType.SELECT)
                        .build();

        PaymentAuthConfig.init(new PaymentAuthConfig.Builder()
                .set3ds2Config(new PaymentAuthConfig.Stripe3ds2Config.Builder().setTimeout(5).setUiCustomization(uiCustomization).build())
                .build());
    }
}

