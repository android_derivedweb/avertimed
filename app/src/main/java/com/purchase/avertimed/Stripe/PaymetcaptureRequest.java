package com.purchase.avertimed.Stripe;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
import static com.purchase.avertimed.Stripe.API.CAPTURE_INTENT;

/**
 * Created by kshitij on 12/17/17.
 */

public class PaymetcaptureRequest extends StringRequest {

    private Map<String, String> parameters;

    public PaymetcaptureRequest(String payment_intent_id, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, API.ROOT_URL+CAPTURE_INTENT, listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("payment_intent_id", payment_intent_id);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
