package com.purchase.avertimed.Stripe;


import com.purchase.avertimed.Stripe.model.BaseModel;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractResponseListener<T extends BaseModel> implements Callback<T> {
    private String message = "";
    private boolean success = false;
    private boolean hasInternetConnectionError = false;

    public abstract void result(T result);

    @Override
    public void onResponse(@NotNull Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            success(response.body());
        } else {
            onFailure(ParsingHelper.getInstance().parseResponse(response));
        }
    }

    @Override
    public void onFailure(@NotNull Call<T> call, Throwable t) {
        t.printStackTrace();
        onError(call, t);
    }

    private void success(T data) {
        if (data != null) {
            message = data.getMessage();
            success = data.isSuccess();
        } else {
            success = false;
        }
        result(data);
    }

    private void onFailure(BaseModel t) {
        if (t != null) {
            message = t.getMessage();
            success = t.isSuccess();
        } else {
            success = false;
        }
        result(null);

    }

    private void onError(Call<T> call, Throwable t) {
        message = t.getLocalizedMessage();
        hasInternetConnectionError = true;
        success = false;
        result(null);


    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean hasInternetConnectionError() {
        return hasInternetConnectionError;
    }
}