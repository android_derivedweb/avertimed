package com.purchase.avertimed.Stripe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Count implements Parcelable {

    private int total_ride;
    private String total_ride_hour;
    private String total_ride_distance;
    private String total_co2_emission_controlled;
    private String total_mountain_climb;
    private String total_calories_burnt;

    public int getTotal_ride() {
        return total_ride;
    }

    public String getTotal_ride_hour() {
        return total_ride_hour;
    }

    public String getTotal_ride_distance() {
        return total_ride_distance;
    }

    public String getTotal_co2_emission_controlled() {
        return total_co2_emission_controlled;
    }

    public String getTotal_mountain_climb() {
        return total_mountain_climb;
    }

    public String getTotal_calories_burnt() {
        return total_calories_burnt;
    }


    protected Count(Parcel in) {
        total_ride = in.readInt();
        total_ride_hour = in.readString();
        total_ride_distance = in.readString();
        total_co2_emission_controlled = in.readString();
        total_mountain_climb = in.readString();
        total_calories_burnt = in.readString();
    }

    public static final Creator<Count> CREATOR = new Creator<Count>() {
        @Override
        public Count createFromParcel(Parcel in) {
            return new Count(in);
        }

        @Override
        public Count[] newArray(int size) {
            return new Count[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(total_ride);
        dest.writeString(total_ride_hour);
        dest.writeString(total_ride_distance);
        dest.writeString(total_co2_emission_controlled);
        dest.writeString(total_mountain_climb);
        dest.writeString(total_calories_burnt);
    }
}
