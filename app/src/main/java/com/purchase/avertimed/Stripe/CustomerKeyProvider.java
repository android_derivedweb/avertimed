package com.purchase.avertimed.Stripe;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Size;

import com.purchase.avertimed.API.UserSession;
import com.purchase.avertimed.Stripe.model.AbstractCallback;
import com.stripe.android.EphemeralKeyProvider;
import com.stripe.android.EphemeralKeyUpdateListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;

public class CustomerKeyProvider implements EphemeralKeyProvider {
    private EphemeralListener ephemeralListener;
    private static String ephemeral;
    UserSession userSession;
    public CustomerKeyProvider(EphemeralListener ephemeralListener, Context context) {
        this.ephemeralListener = ephemeralListener;
        this.userSession = new UserSession(context);

    }

    @Override
    public void createEphemeralKey(@NonNull @Size(min = 4) String apiVersion, @NonNull final EphemeralKeyUpdateListener keyUpdateListener) {


        HashMap<String, String> param = new HashMap<>();
       // param.put("mobile", new PrefManager(CartController.instance).getMobile());
     //   param.put("stripe_customer_id", new PrefManager(CartController.instance).getStripeId());
        Log.e("userSession" , userSession.getAPIToken());
        HashMap<String, String> header = new HashMap<>();
    //    header.put("X-platform", "Android");
        header.put("Accept", "application/json");
        header.put("Authorization", "Bearer " + userSession.getAPIToken());


        StripeCall.getInstance().createKey(header, param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody responseBody) {
                try {

                    //JSONObject jsonObject = new JSONObject(responseBody.string());

                  ///  Log.e("ResponseEmperalKey" , jsonObject.toString());
                   CustomerKeyProvider.ephemeral = responseBody.string();
                 //   EmpherealKeyResponseModel data = new Gson().fromJson(CustomerKeyProvider.ephemeral, EmpherealKeyResponseModel.class);
                 //   new PrefManager(CartController.instance).setStripeId(data.getAssociated_objects().get(0).getId());
                    keyUpdateListener.onKeyUpdate(CustomerKeyProvider.ephemeral);
                    ephemeralListener.onEphemeralUpdate(true, ephemeral);
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        JSONObject jsonObject = new JSONObject(com.purchase.avertimed.Stripe.CustomerKeyProvider.ephemeral);
                        int code = jsonObject.getInt("statusCode");
                        String message = jsonObject.getJSONObject("raw").getString("message");
                        keyUpdateListener.onKeyUpdateFailure(code, message);
                        ephemeralListener.onEphemeralUpdate(false, message);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        ephemeralListener.onEphemeralUpdate(false, e.getMessage());
                    }
                }
            }
        });
    }

    public interface EphemeralListener {
        void onEphemeralUpdate(boolean success, String message);
    }
}