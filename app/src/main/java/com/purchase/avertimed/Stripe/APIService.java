package com.purchase.avertimed.Stripe;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface APIService {

    //Stripe
    /*@POST(API.SUBSCRIPTION_PAYMENT)
    Call<ResponseBody> subscriptionPayment(@Body HashMap<String, Object> param);*/

    @POST(com.purchase.avertimed.Stripe.API.EPHEMERAL_KEY)
    Call<ResponseBody> createKey(@HeaderMap Map<String, String> headers, @Body HashMap<String, String> param);

    @POST(com.purchase.avertimed.Stripe.API.PAYMENT_INTENT)
    Call<ResponseBody> paymentIntent(@HeaderMap Map<String, String> headers, @Body HashMap<String, Object> param);

    @POST(com.purchase.avertimed.Stripe.API.CONFIRM_INTENT)
    Call<ResponseBody> confirmIntent(@HeaderMap Map<String, String> headers, @Body HashMap<String, Object> param);

    @POST(com.purchase.avertimed.Stripe.API.CAPTURE_INTENT)
    Call<ResponseBody> captureIntent(@HeaderMap Map<String, String> headers, @Body HashMap<String, Object> param);

}

