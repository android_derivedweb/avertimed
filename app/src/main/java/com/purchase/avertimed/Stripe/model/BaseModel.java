package com.purchase.avertimed.Stripe.model;

import com.google.gson.annotations.SerializedName;

public class BaseModel {

    private boolean success;
    @SerializedName(value = "msg", alternate = "message")
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
