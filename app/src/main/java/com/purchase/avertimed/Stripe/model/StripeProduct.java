package com.purchase.avertimed.Stripe.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class StripeProduct implements Parcelable {

    private int updated;
    private String type;
    private String name;
    private boolean livemode;
    private List<String> images;
    private int created;
    private List<String> attributes;
    private boolean active;
    private String object;
    private String id;

    protected StripeProduct(Parcel in) {
        updated = in.readInt();
        type = in.readString();
        name = in.readString();
        livemode = in.readByte() != 0;
        images = in.createStringArrayList();
        created = in.readInt();
        attributes = in.createStringArrayList();
        active = in.readByte() != 0;
        object = in.readString();
        id = in.readString();
    }

    public static final Creator<StripeProduct> CREATOR = new Creator<StripeProduct>() {
        @Override
        public StripeProduct createFromParcel(Parcel in) {
            return new StripeProduct(in);
        }

        @Override
        public StripeProduct[] newArray(int size) {
            return new StripeProduct[size];
        }
    };

    public int getUpdated() {
        return updated;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean getLivemode() {
        return livemode;
    }

    public List<String> getImages() {
        return images;
    }

    public int getCreated() {
        return created;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public boolean getActive() {
        return active;
    }

    public String getObject() {
        return object;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(updated);
        dest.writeString(type);
        dest.writeString(name);
        dest.writeByte((byte) (livemode ? 1 : 0));
        dest.writeStringList(images);
        dest.writeInt(created);
        dest.writeStringList(attributes);
        dest.writeByte((byte) (active ? 1 : 0));
        dest.writeString(object);
        dest.writeString(id);
    }
}
