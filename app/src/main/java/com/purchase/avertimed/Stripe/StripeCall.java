package com.purchase.avertimed.Stripe;


import com.purchase.avertimed.Stripe.model.AbstractCallback;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;

public class StripeCall {

    private static com.purchase.avertimed.Stripe.StripeCall instance = new com.purchase.avertimed.Stripe.StripeCall();
    private APIService apiService;

    private StripeCall() {
        apiService = com.purchase.avertimed.Stripe.RetrofitFactory.getApiService(APIService.class);
    }

    public static com.purchase.avertimed.Stripe.StripeCall getInstance() {
        return instance;
    }


    public void createKey(Map<String, String> header, HashMap<String, String> param, AbstractCallback<ResponseBody> callback) {
        apiService.createKey(header, param).enqueue(callback);
    }

    public void paymentIntent(Map<String, String> header, HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.paymentIntent(header, param).enqueue(callback);
    }

    public void confirmIntent(Map<String, String> header, HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.confirmIntent(header, param).enqueue(callback);
    }

    public void captureIntent(Map<String, String> header, HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.captureIntent(header, param).enqueue(callback);
    }

    /*public void createRenewal(HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.createRenewal(param).enqueue(callback);
    }

    public void cancelRenewal(HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.cancelRenewal(param).enqueue(callback);
    }*/

   /* public void addCard(Card card, AbstractCallback<ObjectBaseModel<CardBase>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", AppPreferences.INSTANCE.getUserID());
        param.put("number", card.getNumber());
        param.put("exp_month", String.valueOf(card.getExp_month()));
        param.put("exp_year", String.valueOf(card.getExp_year()));
        param.put("cvc", card.getCvc());
        if (card.getId().isEmpty()) {
            apiService.addCard(param).enqueue(callback);
        } else {
            param.put("card_id", card.getId());
            apiService.updateCard(param).enqueue(callback);
        }
    }

    public void setDefault(Card card, AbstractCallback<ObjectBaseModel<CardBase>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", AppPreferences.INSTANCE.getUserID());
        param.put("card_id", card.getId());
        apiService.setDefault(param).enqueue(new AbstractCallback<ObjectBaseModel<CardBase>>() {
            @Override
            public void result(ObjectBaseModel<CardBase> result) {
                if (callback != null) callback.result(result);
            }
        });
    }

    public void getCustomer(AbstractCallback<ObjectBaseModel<Customer>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", AppPreferences.INSTANCE.getUserID());
        apiService.getCustomer(param).enqueue(callback);
    }

    public void deleteCard(Card card, AbstractCallback<ObjectBaseModel<CardBase>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id",AppPreferences.INSTANCE.getUserID());
        param.put("cardId", card.getId());
        apiService.deleteCard(param).enqueue(callback);
    }

    public void getCards(AbstractCallback<ObjectBaseModel<Customer>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", AppPreferences.INSTANCE.getUserID());
//        apiService.listCard(param).enqueue(callback);
        apiService.getCustomer(param).enqueue(new AbstractCallback<ObjectBaseModel<Customer>>() {
            @Override
            public void result(ObjectBaseModel<Customer> result) {
                if (callback != null) callback.result(result);
            }
        });
    }


   *//* public void purchasePackage(String intent_id, String lock_id, Package aPackage, String type, AbstractResponseListener<ObjectBaseModel<Package>> callback) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("intent_id", intent_id);
        param.put("user_id", AppPreferences.INSTANCE.getUserID());
        param.put("package_type", type);
        param.put("package_id", aPackage.getId());
        param.put("currency", aPackage.getCurrency());
        param.put("chargeableAmount", aPackage.getAmount());
        param.put("description", aPackage.getDescription());
        param.put("lock_id", lock_id);

        apiService.purchaseRental(param).enqueue(callback);
    }
*/
    /*public void getTransactions(int page, int limit, AbstractResponseListener<ListBaseModel<Transaction>> callback) {
        HashMap<String, Object> param = new HashMap<>();
        param.put(Parameter.user_id, AppPreferences.INSTANCE.getUserID());
        param.put("page", page);
        param.put("limit", limit);

        Call<ListBaseModel<Transaction>> call = apiService.getTransactions(param);
        call.enqueue(callback);
    }*/


   /* public void subscriptionPayment(HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.subscriptionPayment(param).enqueue(callback);
    }*/
}
