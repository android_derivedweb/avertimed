package com.purchase.avertimed.Stripe;

public class Parameter {

    public static final String email = "email";
    public static final String password = "password";
    public static final String fcm_token = "fcm_token";
    public static final String device_type = "device_type";
    public static final String device_id = "device_id";
    public static final String app_id = "app_id";
    public static final String username = "username";
    public static final String name = "name";
    public static final String mobile = "mobile";
    public static final String status = "user_profile_status";
    public static final String user_image = "user_image";
    public static final String otp = "otp";
    public static final String new_password = "new_password";
    public static final String confirm_password = "confirm_password";
    public static final String password_otp = "password_otp";
    public static final String user_profile_image = "user_profile_image";
    public static final String user_id = "user_id";
    public static final String req_type = "req_type";
    public static final String search = "search";
    public static final String lock_id = "lock_id";
    public static final String is_login = "is_login";

    public static final String purchased_package = "purchased_package";

    public static final String profile_email = "profile_email";
    public static final String country_code = "country_code";
    public static final String country = "country";

    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String distance = "distance";
}