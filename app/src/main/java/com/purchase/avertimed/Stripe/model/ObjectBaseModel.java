package com.purchase.avertimed.Stripe.model;

public class ObjectBaseModel<T> extends BaseModel {
    private T data;
    private boolean is_otp_verified;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isIs_otp_verified() {
        return is_otp_verified;
    }

    public void setIs_otp_verified(boolean is_otp_verified) {
        this.is_otp_verified = is_otp_verified;
    }
}
