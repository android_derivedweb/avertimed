package com.purchase.avertimed.Stripe.model;

import java.util.List;

public class StripeLeaseResponse {

    private String billing;
    private int webhooks_delivered_at;
    private List<String> total_tax_amounts;
    private List<String> total_discount_amounts;
    private int total;
    private int subtotal;
    private String subscription;
    private Status_transitions status_transitions;
    private String status;
    private int starting_balance;
    private int pre_payment_credit_notes_amount;
    private int post_payment_credit_notes_amount;
    private int period_start;
    private int period_end;
    private String payment_intent;
    private boolean paid;
    private String number;
    private boolean livemode;
    private Lines lines;
    private String invoice_pdf;
    private String hosted_invoice_url;
    private int ending_balance;
    private List<String> discounts;
    private List<String> default_tax_rates;
    private List<String> customer_tax_ids;
    private String customer_tax_exempt;
    private String customer;
    private String currency;
    private int created;
    private String collection_method;
    private String charge;
    private String billing_reason;
    private boolean auto_advance;
    private boolean attempted;
    private int attempt_count;
    private int amount_remaining;
    private int amount_paid;
    private int amount_due;
    private String account_country;
    private String object;
    private String id;

    public String getDescription() {
        if (lines == null) return "";
        if (lines.getData() == null || lines.getData().size() <= 0) return "";
        return lines.getData().get(0).getDescription();
    }

    public String getBilling() {
        return billing;
    }

    public int getWebhooks_delivered_at() {
        return webhooks_delivered_at;
    }

    public List<String> getTotal_tax_amounts() {
        return total_tax_amounts;
    }

    public List<String> getTotal_discount_amounts() {
        return total_discount_amounts;
    }

    public int getTotal() {
        return total;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public String getSubscription() {
        return subscription;
    }

    public Status_transitions getStatus_transitions() {
        return status_transitions;
    }

    public String getStatus() {
        return status;
    }

    public int getStarting_balance() {
        return starting_balance;
    }

    public int getPre_payment_credit_notes_amount() {
        return pre_payment_credit_notes_amount;
    }

    public int getPost_payment_credit_notes_amount() {
        return post_payment_credit_notes_amount;
    }

    public int getPeriod_start() {
        return period_start;
    }

    public int getPeriod_end() {
        return period_end;
    }

    public String getPayment_intent() {
        return payment_intent;
    }

    public boolean getPaid() {
        return paid;
    }

    public String getNumber() {
        return number;
    }

    public boolean getLivemode() {
        return livemode;
    }

    public Lines getLines() {
        return lines;
    }

    public String getInvoice_pdf() {
        return invoice_pdf;
    }

    public String getHosted_invoice_url() {
        return hosted_invoice_url;
    }

    public int getEnding_balance() {
        return ending_balance;
    }

    public List<String> getDiscounts() {
        return discounts;
    }

    public List<String> getDefault_tax_rates() {
        return default_tax_rates;
    }

    public List<String> getCustomer_tax_ids() {
        return customer_tax_ids;
    }

    public String getCustomer_tax_exempt() {
        return customer_tax_exempt;
    }

    public String getCustomer() {
        return customer;
    }

    public String getCurrency() {
        return currency;
    }

    public int getCreated() {
        return created;
    }

    public String getCollection_method() {
        return collection_method;
    }

    public String getCharge() {
        return charge;
    }

    public String getBilling_reason() {
        return billing_reason;
    }

    public boolean getAuto_advance() {
        return auto_advance;
    }

    public boolean getAttempted() {
        return attempted;
    }

    public int getAttempt_count() {
        return attempt_count;
    }

    public int getAmount_remaining() {
        return amount_remaining;
    }

    public int getAmount_paid() {
        return amount_paid;
    }

    public int getAmount_due() {
        return amount_due;
    }

    public String getAccount_country() {
        return account_country;
    }

    public String getObject() {
        return object;
    }

    public String getId() {
        return id;
    }

    public double getTransactionAmount() {
        if (amount_paid <= 0) return 0;
        return amount_paid / 100f;
    }

    public static class Status_transitions {
        private int paid_at;
        private int finalized_at;

        public int getPaid_at() {
            return paid_at;
        }

        public int getFinalized_at() {
            return finalized_at;
        }
    }

    public static class Lines {
        private String url;
        private int total_count;
        private boolean has_more;
        private List<Data> data;
        private String object;

        public String getUrl() {
            return url;
        }

        public int getTotal_count() {
            return total_count;
        }

        public boolean getHas_more() {
            return has_more;
        }

        public List<Data> getData() {
            return data;
        }

        public String getObject() {
            return object;
        }
    }

    public static class Data {
        private String unique_id;
        private String type;
        private List<String> tax_rates;
        private List<String> tax_amounts;
        private String subscription_item;
        private String subscription;
        private int quantity;
        private boolean proration;
        private Price price;
        private Plan plan;
        private Period period;
        private Metadata metadata;
        private boolean livemode;
        private List<String> discounts;
        private boolean discountable;
        private List<String> discount_amounts;
        private String description;
        private String currency;
        private int amount;
        private String object;
        private String id;

        public String getUnique_id() {
            return unique_id;
        }

        public String getType() {
            return type;
        }

        public List<String> getTax_rates() {
            return tax_rates;
        }

        public List<String> getTax_amounts() {
            return tax_amounts;
        }

        public String getSubscription_item() {
            return subscription_item;
        }

        public String getSubscription() {
            return subscription;
        }

        public int getQuantity() {
            return quantity;
        }

        public boolean getProration() {
            return proration;
        }

        public Price getPrice() {
            return price;
        }

        public Plan getPlan() {
            return plan;
        }

        public Period getPeriod() {
            return period;
        }

        public Metadata getMetadata() {
            return metadata;
        }

        public boolean getLivemode() {
            return livemode;
        }

        public List<String> getDiscounts() {
            return discounts;
        }

        public boolean getDiscountable() {
            return discountable;
        }

        public List<String> getDiscount_amounts() {
            return discount_amounts;
        }

        public String getDescription() {
            return description;
        }

        public String getCurrency() {
            return currency;
        }

        public int getAmount() {
            return amount;
        }

        public String getObject() {
            return object;
        }

        public String getId() {
            return id;
        }
    }

    public static class Price {
        private String unit_amount_decimal;
        private int unit_amount;
        private String type;
        private Recurring recurring;
        private String product;
        private boolean livemode;
        private String currency;
        private int created;
        private String billing_scheme;
        private boolean active;
        private String object;
        private String id;

        public String getUnit_amount_decimal() {
            return unit_amount_decimal;
        }

        public int getUnit_amount() {
            return unit_amount;
        }

        public String getType() {
            return type;
        }

        public Recurring getRecurring() {
            return recurring;
        }

        public String getProduct() {
            return product;
        }

        public boolean getLivemode() {
            return livemode;
        }

        public String getCurrency() {
            return currency;
        }

        public int getCreated() {
            return created;
        }

        public String getBilling_scheme() {
            return billing_scheme;
        }

        public boolean getActive() {
            return active;
        }

        public String getObject() {
            return object;
        }

        public String getId() {
            return id;
        }
    }

    public static class Recurring {
        private String usage_type;
        private int interval_count;
        private String interval;

        public String getUsage_type() {
            return usage_type;
        }

        public int getInterval_count() {
            return interval_count;
        }

        public String getInterval() {
            return interval;
        }
    }

    public static class Plan {
        private String usage_type;
        private String product;
        private boolean livemode;
        private int interval_count;
        private String interval;
        private String currency;
        private int created;
        private String billing_scheme;
        private String amount_decimal;
        private int amount;
        private boolean active;
        private String object;
        private String id;

        public String getUsage_type() {
            return usage_type;
        }

        public String getProduct() {
            return product;
        }

        public boolean getLivemode() {
            return livemode;
        }

        public int getInterval_count() {
            return interval_count;
        }

        public String getInterval() {
            return interval;
        }

        public String getCurrency() {
            return currency;
        }

        public int getCreated() {
            return created;
        }

        public String getBilling_scheme() {
            return billing_scheme;
        }

        public String getAmount_decimal() {
            return amount_decimal;
        }

        public int getAmount() {
            return amount;
        }

        public boolean getActive() {
            return active;
        }

        public String getObject() {
            return object;
        }

        public String getId() {
            return id;
        }
    }

    public static class Period {
        private int start;
        private int end;

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }
    }

    public static class Metadata {
        private String lock_id;
        private String customer;
        private String statement_descriptor_suffix;
        private String currency;
        private String user_id;
        private String package_type;
        private String package_id;
        private String source;
        private String description;
        private String price_id;
        private String destination;
        private String amount;
        private String purchase_type;
        private String capture_method;

        public String getLock_id() {
            return lock_id;
        }

        public String getCustomer() {
            return customer;
        }

        public String getStatement_descriptor_suffix() {
            return statement_descriptor_suffix;
        }

        public String getCurrency() {
            return currency;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getPackage_type() {
            return package_type;
        }

        public String getPackage_id() {
            return package_id;
        }

        public String getSource() {
            return source;
        }

        public String getDescription() {
            return description;
        }

        public String getPrice_id() {
            return price_id;
        }

        public String getDestination() {
            return destination;
        }

        public String getAmount() {
            return amount;
        }

        public String getPurchase_type() {
            return purchase_type;
        }

        public String getCapture_method() {
            return capture_method;
        }
    }
}
