package com.purchase.avertimed.Stripe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class StripePrice implements Parcelable {

    private String unit_amount_decimal;
    private int unit_amount;
    private String type;
    private Recurring recurring;
    private String product;
    private boolean livemode;
    private String currency;
    private int created;
    private String billing_scheme;
    private boolean active;
    private String object;
    private String id;

    protected StripePrice(Parcel in) {
        unit_amount_decimal = in.readString();
        unit_amount = in.readInt();
        type = in.readString();
        product = in.readString();
        livemode = in.readByte() != 0;
        currency = in.readString();
        created = in.readInt();
        billing_scheme = in.readString();
        active = in.readByte() != 0;
        object = in.readString();
        id = in.readString();
    }

    public static final Creator<StripePrice> CREATOR = new Creator<StripePrice>() {
        @Override
        public StripePrice createFromParcel(Parcel in) {
            return new StripePrice(in);
        }

        @Override
        public StripePrice[] newArray(int size) {
            return new StripePrice[size];
        }
    };

    public String getUnit_amount_decimal() {
        return unit_amount_decimal;
    }

    public int getUnit_amount() {
        return unit_amount;
    }

    public String getType() {
        return type;
    }

    public Recurring getRecurring() {
        return recurring;
    }

    public String getProduct() {
        return product;
    }

    public boolean getLivemode() {
        return livemode;
    }

    public String getCurrency() {
        return currency;
    }

    public int getCreated() {
        return created;
    }

    public String getBilling_scheme() {
        return billing_scheme;
    }

    public boolean getActive() {
        return active;
    }

    public String getObject() {
        return object;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(unit_amount_decimal);
        dest.writeInt(unit_amount);
        dest.writeString(type);
        dest.writeString(product);
        dest.writeByte((byte) (livemode ? 1 : 0));
        dest.writeString(currency);
        dest.writeInt(created);
        dest.writeString(billing_scheme);
        dest.writeByte((byte) (active ? 1 : 0));
        dest.writeString(object);
        dest.writeString(id);
    }

    public static class Recurring {
        private String usage_type;
        private int interval_count;
        private String interval;

        public String getUsage_type() {
            return usage_type;
        }

        public int getInterval_count() {
            return interval_count;
        }

        public String getInterval() {
            return interval;
        }
    }
}
