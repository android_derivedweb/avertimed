package com.purchase.avertimed.Stripe;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.activity.ComponentActivity;
import androidx.annotation.Nullable;

import com.purchase.avertimed.API.UserSession;
import com.purchase.avertimed.Stripe.model.AbstractCallback;
import com.google.gson.Gson;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.PaymentSession;
import com.stripe.android.PaymentSessionData;
import com.stripe.android.SetupIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.ConfirmSetupIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.StripeIntent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;

public class PaymentSessionHandler implements PaymentSession.PaymentSessionListener {

    public static final String TYPE_PACKAGE = "package";
    private final UserSession userSession;

    private String destination_stripe_account_id = null;

    private Activity activity;

    public String description;
    private double amount;
    private String clientSecret;
    private String payment_method_id;
    private PaymentSession paymentSession;
    private PaymentSessionListener paymentSessionListener;

    private boolean user_initiated = false;

    PaymentSessionHandler(Activity activity) {
        this.activity = activity;
        this.userSession = new UserSession(activity);
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setPaymentSessionListener(PaymentSessionListener paymentSessionListener) {
        this.paymentSessionListener = paymentSessionListener;
    }

    public void setDestination_stripe_account_id(String destination_stripe_account_id) {
        this.destination_stripe_account_id = destination_stripe_account_id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void initTransaction(double amount) {
        this.amount = amount;
        com.purchase.avertimed.Stripe.StripeUtil.applyTheme(activity);
        userSession.showProgressDialog(activity);
        if (paymentSession != null) paymentSession.clearPaymentMethod();
        initPaymentSession();
    }

    private void initPaymentSession() {
        user_initiated = false;
        PaymentConfiguration.init(activity, com.purchase.avertimed.Stripe.StripeUtil.STRIPE_PUBLISHER_KEY);
        CustomerSession.initCustomerSession(activity, new com.purchase.avertimed.Stripe.CustomerKeyProvider((success, message) -> {
            userSession.hideProgressDialog();
            if (success) {
                paymentSession = new PaymentSession((ComponentActivity) activity, com.purchase.avertimed.Stripe.StripeUtil.defaultPaymentSessionConfig(activity));
                paymentSession.clearPaymentMethod();
                paymentSession.init(this);
                paymentSession.setCartTotal((long) amount);
                paymentSession.presentPaymentMethodSelection("Stripe");
            } else {
                paymentSessionListener.onPaymentFailed(message);
                clearSession();
            }
        },activity));
    }

    private Stripe getStripe() {
        return com.purchase.avertimed.Stripe.StripeUtil.getStripe(activity, null);
    }

    boolean isResultFailure = false;

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (intent == null) return;
        if (paymentSession == null) return;

        isResultFailure = false;

        boolean isPaymentIntentResult = getStripe().onPaymentResult(requestCode, intent, new ApiResultCallback<PaymentIntentResult>() {
            @Override
            public void onSuccess(@NotNull PaymentIntentResult paymentIntentResult) {
                userSession.hideProgressDialog();
                processStripeIntent(paymentIntentResult.getIntent(), true);
            }

            @Override
            public void onError(@NotNull Exception e) {

                Log.e("Error1",""+e.getLocalizedMessage());
                e.printStackTrace();
                userSession.hideProgressDialog();
                isResultFailure = true;
                paymentSessionListener.onPaymentFailed(e.getMessage());
            }
        });

        if (isPaymentIntentResult && !isResultFailure) {
            userSession.showProgressDialog(activity);
        } else {
            boolean isSetupIntentResult = getStripe().onSetupResult(requestCode, intent, new ApiResultCallback<SetupIntentResult>() {
                @Override
                public void onSuccess(@NotNull SetupIntentResult setupIntentResult) {
                    userSession.hideProgressDialog();
                    processStripeIntent(setupIntentResult.getIntent(), true);
                }

                @Override
                public void onError(@NotNull Exception e) {
                    Log.e("Error2",e.getMessage());
                    e.printStackTrace();
                    userSession.hideProgressDialog();
                    paymentSessionListener.onPaymentFailed(e.getMessage());
                }
            });
            if (!isSetupIntentResult) {
                paymentSession.handlePaymentData(requestCode, resultCode, intent);
            }
        }

    }


    private void processStripeIntent(StripeIntent stripeIntent, boolean isAfterConfirmation) {
        if (stripeIntent == null) return;
        confirmStripeIntent(stripeIntent.getId(), "");
        if (stripeIntent.requiresAction()) {
            getStripe().handleNextActionForPayment(activity, Objects.requireNonNull(stripeIntent.getClientSecret()));
        } else if (stripeIntent.requiresConfirmation()) {
            confirmStripeIntent(stripeIntent.getId(), destination_stripe_account_id);
        } else if (stripeIntent.getStatus() == PaymentIntent.Status.Succeeded) {
            if (stripeIntent instanceof PaymentIntent) {
                PaymentIntent paymentIntent = (PaymentIntent) stripeIntent;
                Log.e("Payment", new Gson().toJson(stripeIntent));
                paymentSessionListener.onPaymentSuccess(/*paymentIntent.getId()*/new Gson().toJson(stripeIntent), true);
                clearSession();
            } else if (stripeIntent instanceof SetupIntent) {
                SetupIntent setupIntent = (SetupIntent) stripeIntent;
                Log.e("Payment", new Gson().toJson(stripeIntent));
                paymentSessionListener.onPaymentSuccess(/*setupIntent.getId()*/new Gson().toJson(stripeIntent), true);
                clearSession();
            }
        } else if (stripeIntent.getStatus() == PaymentIntent.Status.RequiresPaymentMethod) {
            if (isAfterConfirmation) {
                paymentSessionListener.onPaymentFailed("Payment method not authorised or payment has been declined, Please contact your payment bank for more info.");
                clearSession();
            } else {
                if (payment_method_id == null) return;
                if (stripeIntent instanceof PaymentIntent) {
                    getStripe().confirmPayment(activity, ConfirmPaymentIntentParams.createWithPaymentMethodId(payment_method_id, clientSecret));
                } else if (stripeIntent instanceof SetupIntent) {
                    getStripe().confirmSetupIntent(activity, ConfirmSetupIntentParams.create(payment_method_id, clientSecret));
                }
            }
        } else if (stripeIntent.getStatus() == PaymentIntent.Status.RequiresCapture) {
            Log.e("Payment", new Gson().toJson(stripeIntent));
            paymentSessionListener.onPaymentSuccess(/*stripeIntent.getId()*/new Gson().toJson(stripeIntent), false);
            clearSession();
            // We suppose to capture the payment, It will be kept reserved for maximum 7 days..
            // Capture intent will be done in end ride api.
        } else {
            paymentSessionListener.onPaymentFailed("Unhandled payment intent, Status - " + stripeIntent.getStatus().toString());
        }
    }


    private void confirmStripeIntent(String payment_intent_id, String stripeAccountId) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("payment_intent_id", payment_intent_id);
        if (!stripeAccountId.isEmpty()) param.put("stripe_account", stripeAccountId);

        HashMap<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");
        header.put("Authorization", "Bearer " + userSession.getAPIToken());


        userSession.showProgressDialog(activity);
        com.purchase.avertimed.Stripe.StripeCall.getInstance().confirmIntent(header, param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                userSession.hideProgressDialog();
                try {
                    JSONObject response = new JSONObject(result.string());
                    if (response.has("success")) {
                        boolean success = response.getBoolean("success");
                        if (success) {
                            Log.e("confirmIntent", new Gson().toJson(response));
                            paymentSessionListener.onPaymentSuccess(/*payment_intent_id*/new Gson().toJson(response), true);
                        } else {
                            String message = response.getJSONObject("raw").getString("message");
                            paymentSessionListener.onPaymentFailed(message);
                        }
                        clearSession();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    paymentSessionListener.onPaymentFailed("Payment failed;");

                }
            }
        });
    }

    private void clearSession() {
        if (paymentSession == null) return;
        paymentSession.clearPaymentMethod();
        paymentSession = null;
    }

    @Override
    public void onCommunicatingStateChanged(boolean isCommunicating) {
        Log.e("PaymentSession", "onCommunicatingStateChanged : " + isCommunicating);

    }

    @Override
    public void onError(int errorCode, @NotNull String errorMessage) {
        Log.e("PaymentSession", "onError : " + errorMessage);
        paymentSessionListener.onPaymentFailed(errorMessage);
        clearSession();

    }

    @Override
    public void onPaymentSessionDataChanged(@NotNull PaymentSessionData data) {
        Log.e("PaymentSession", "onPaymentSessionDataChanged : " + data.toString());
        PaymentMethod paymentMethod = data.getPaymentMethod();


        if (paymentMethod == null || paymentMethod.id == null) return;
        if (!data.isPaymentReadyToCharge()) return;
        if (user_initiated) return;
        user_initiated = true;
        userSession.showProgressDialog(activity);
        payment_method_id = paymentMethod.id;
        HashMap<String, Object> param = new HashMap<>();
        param.put("amount", amount);
//        param.put("currency", "usd");
     //   param.put("customer_id", new PrefManager(activity).getStripeId());
    //    param.put("source", payment_method_id);
//        param.put("capture_method", "manual");
//        param.put("description", "Payment for registration");
//        param.put("statement_descriptor_suffix", "Registration");
//        param.put("payment_method_types", new String[]{"card"});

        HashMap<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");
        header.put("Authorization", "Bearer " + userSession.getAPIToken());

        com.purchase.avertimed.Stripe.StripeCall.getInstance().paymentIntent(header, param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                userSession.hideProgressDialog();
                try {
                    String response = result.string();
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("paymentIntent",response);

                    if (jsonObject.has("client_secret")) {
                        clientSecret = jsonObject.getString("client_secret");
                        payment_method_id = jsonObject.getString("payment_method");
                        getStripe().confirmPayment(activity, ConfirmPaymentIntentParams.createWithPaymentMethodId(payment_method_id, clientSecret));
                    } else {
                        Toast.makeText(activity,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                        String message = jsonObject.getString("msg");
                        paymentSessionListener.onPaymentFailed(message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private StripeIntent retrieveStripeIntent(String clientSecret) {
        StripeIntent stripeIntent = null;
        try {
            if (clientSecret.startsWith("pi_")) {
                stripeIntent = getStripe().retrievePaymentIntentSynchronous(clientSecret);
            } else if (clientSecret.startsWith("seti_")) {
                stripeIntent = getStripe().retrieveSetupIntentSynchronous(clientSecret);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stripeIntent;
    }

    private void finishPayment() {
        paymentSession.onCompleted();
        paymentSession.clearPaymentMethod();
        paymentSession = null;
    }

    private void finishSetup() {
        paymentSession.onCompleted();
        paymentSession.clearPaymentMethod();
        paymentSession = null;
    }

    public interface PaymentSessionListener {
        void onPaymentSuccess(String payment_intent_id, boolean captured);

        void onPaymentFailed(String message);
    }
}
