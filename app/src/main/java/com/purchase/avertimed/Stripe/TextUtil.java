package com.purchase.avertimed.Stripe;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.text.util.Linkify;
import android.util.Patterns;
import android.widget.TextView;

import java.util.Locale;

public class TextUtil {
    public static boolean isNullOrEmpty(CharSequence charSequence) {
        if (charSequence == null) return true;
        return charSequence.length() <= 0;
    }

    public static boolean isNullOrEmpty(String string) {
        if (string == null) return true;
        return string.length() <= 0;
    }

    public static boolean isNullOrEmpty(TextView textView) {
        if (textView == null) return true;
        if (textView.getText() == null) return true;
        return isNullOrEmpty(textView.getText().toString().trim());
    }

    public static boolean isValidEmail(CharSequence target) {
        return !isNullOrEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void linkify(TextView txtMsg) {
        Linkify.addLinks(txtMsg, Linkify.ALL);
    }

    public static double parseDouble(String value) {
        if (isNullOrEmpty(value)) return 0;
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            return 0;
        }
    }

    public static long parseLong(String value) {
        if (isNullOrEmpty(value)) return 0;
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            return 0;
        }
    }

    public static int parseInt(String raw) {
        if (TextUtil.isNullOrEmpty(raw)) return 0;
        try {
            return Integer.parseInt(raw);
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean parseBoolean(String raw) {
        if (isNullOrEmpty(raw)) return false;
        if (raw.contains("1") || raw.contains("0")) {
            if (raw.contains("1")) return true;
            if (raw.contains("0")) return false;
        } else return raw.toLowerCase().contains("true");
        return false;
    }

    public static String getLastCharacters(String word, int count) {
        if (word.length() == count) return word;
        return word.length() > count ? word.substring(word.length() - count) : "";

    }

    public static void addStrike(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static void removeStrike(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
    }

    public static String getZeroDecimalPrice(double value) {
        return String.format(Locale.ENGLISH, "%.0f", value);
    }

    public static String getTwoDecimalPrice(double value) {
        return String.format(Locale.ENGLISH, "%.02f", value);
    }

    public static String getTwoDecimalPrice(String value) {
        return getTwoDecimalPrice(parseDouble(value));
    }


    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        return Bitmap.createBitmap(image, 0, 0, width, height, matrix, false);
    }
}
