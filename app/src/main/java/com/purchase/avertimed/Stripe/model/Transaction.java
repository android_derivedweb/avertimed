package com.purchase.avertimed.Stripe.model;

import com.google.gson.annotations.SerializedName;

public class Transaction {


    private static final String SUBSCRIPTION_PURCHASED = "subscription_purchase";
    private static final String PACKAGE_PAYMENT = "package_payment";
    private static final String RENT_PACKAGE_PURCHASED = "package_purchase";
    private static final String LEASE_PACKAGE_PURCHASED = "lease_package_purchase";
    private static final String SALE_PACKAGE_PURCHASED = "sale_package_purchase";

    private String _id;
    private String type;
    @SerializedName("updatedAt")
    private String updated_at;
    @SerializedName("createdAt")
    private String created_at;

    private String transaction_type;
    /*@SerializedName("operator_id")
    private Person operator;
    private Person from_user;*/

    @SerializedName(value = "stripe_response", alternate = "stripe_intent_response")
    private StripeResponse stripe_response;
    @SerializedName("lease_stripe_response")
    private StripeLeaseResponse stripe_lease_response;


    public String get_id() {
        return _id;
    }

    public String getType() {
        return type;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public StripeResponse getStripe_response() {
        return stripe_response;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public String getTitle() {
        if (transaction_type == null) return "";
        if (transaction_type.equalsIgnoreCase(SUBSCRIPTION_PURCHASED)) {
            return "Subscription Purchased";
        } else if (transaction_type.equalsIgnoreCase(PACKAGE_PAYMENT)) {
            return "Received Payment";
        } else if (transaction_type.equalsIgnoreCase(RENT_PACKAGE_PURCHASED)) {
            return "Purchased - Rent Package";
        } else if (transaction_type.equalsIgnoreCase(LEASE_PACKAGE_PURCHASED)) {
            return "Purchased - Lease Package";
        } else if (transaction_type.equalsIgnoreCase(SALE_PACKAGE_PURCHASED)) {
            return "Purchased - Sale Package";
        } else {
            return transaction_type;
        }
    }

    public boolean isDeducted() {
        if (type == null) return true;
        return !type.equalsIgnoreCase("credit");
    }

    public double getAmount() {
        if (stripe_lease_response != null) return stripe_lease_response.getTransactionAmount();
        if (stripe_response != null) return stripe_response.getTransactionAmount();
        return 0;
    }

    public String getCurrency() {
        if (stripe_lease_response != null) return stripe_lease_response.getCurrency().toUpperCase();
        if (stripe_response != null) return stripe_response.getCurrency().toUpperCase();
        return "";
    }

    public String getSummary() {
        if (stripe_lease_response != null) {
            return stripe_lease_response.getDescription();
        } else if (stripe_response != null) {
            return hasError() ? stripe_response.getError().getMessage()
                    : stripe_response.getDescription();
        } else {
            return "";
        }
    }

    public boolean hasError() {
        if (stripe_lease_response != null) return false;
        return stripe_response != null && stripe_response.getError() != null;
    }

    public String getStatus() {
        StripeResponse settlement = getStripe_response();
        if (settlement == null) return "";
        return settlement.getStatus();
    }

    public boolean hasReceiptUrl() {
        if (stripe_lease_response != null && !com.purchase.avertimed.Stripe.TextUtil.isNullOrEmpty(stripe_lease_response.getHosted_invoice_url()))
            return true;
        return stripe_response != null && !com.purchase.avertimed.Stripe.TextUtil.isNullOrEmpty(stripe_response.getReceiptUrl());
    }

    public String getReceiptUrl() {
        if (hasReceiptUrl()) {
            if (stripe_lease_response != null) return stripe_lease_response.getHosted_invoice_url();
            else return stripe_response.getReceiptUrl();
        } else return "";
    }
}