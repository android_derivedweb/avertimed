package com.purchase.avertimed.Stripe;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {
    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .build();
    }


    private static Retrofit getInstance(String url) {
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(getClient())
//                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static <T> T getApiService(final String root_url, final Class<T> service) {
        return getInstance(root_url).create(service);
    }

    public static <T> T getApiService(final Class<T> service) {
        return getApiService(API.ROOT_URL, service);
    }
}
