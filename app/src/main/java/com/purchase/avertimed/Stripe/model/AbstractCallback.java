package com.purchase.avertimed.Stripe.model;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractCallback<T> implements Callback<T> {
    private String message = "";

    public abstract void result(T result);

    @Override
    public void onResponse(@NotNull Call<T> call, @NotNull Response<T> response) {
        result(response.body());
    }

    @Override
    public void onFailure(@NotNull Call<T> call, @NotNull Throwable t) {
        message = t.getLocalizedMessage();
        result(null);
    }
}