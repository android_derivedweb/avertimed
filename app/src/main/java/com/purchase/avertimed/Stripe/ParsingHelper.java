package com.purchase.avertimed.Stripe;

import android.util.Log;

import com.google.gson.Gson;
import com.purchase.avertimed.Stripe.model.BaseModel;

import retrofit2.Response;

public class ParsingHelper {
    private static final com.purchase.avertimed.Stripe.ParsingHelper ourInstance = new com.purchase.avertimed.Stripe.ParsingHelper();

    private ParsingHelper() {

    }

    public static com.purchase.avertimed.Stripe.ParsingHelper getInstance() {
        return ourInstance;
    }

    public BaseModel parseResponse(String response) throws NullPointerException {
        try {
            Log.e("response: ", response);
            return new Gson().fromJson(response, BaseModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> BaseModel parseResponse(Response<T> response) throws NullPointerException {
        if (response.errorBody() == null) return null;
        try {
            String error = response.errorBody().string();
            Log.e("Error", error);
            return new Gson().fromJson(error, BaseModel.class);
        } catch (Exception e) {
            try {
                BaseModel baseModel = new BaseModel();
                baseModel.setSuccess(false);
                baseModel.setMessage("Code - " + response.code() + ", Error : " + response.message());
                return baseModel;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }
    }


}
