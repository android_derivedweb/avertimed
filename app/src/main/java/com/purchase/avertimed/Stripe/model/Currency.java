package com.purchase.avertimed.Stripe.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Currency implements Parcelable {
    private String symbol;
    private String name;
    private String code;

    protected Currency(Parcel in) {
        symbol = in.readString();
        name = in.readString();
        code = in.readString();
    }

    public static final Creator<Currency> CREATOR = new Creator<Currency>() {
        @Override
        public Currency createFromParcel(Parcel in) {
            return new Currency(in);
        }

        @Override
        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(symbol);
        dest.writeString(name);
        dest.writeString(code);
    }
}