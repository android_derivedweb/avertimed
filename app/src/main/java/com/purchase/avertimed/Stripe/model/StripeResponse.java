package com.purchase.avertimed.Stripe.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StripeResponse {

    private String object;
    private String id;
    private int invoice_number;
    private String status;

    private String payment_method;
    private boolean livemode;
    private String currency;
    private String description;
    private String confirmation_method;
    private String client_secret;
    private Charges charges;
    private String capture_method;
    private String application;
    private long amount_received;
    private long amount_capturable;
    private long amount;
    private int created;
    private List<String> payment_method_types;
    private Payment_method_options payment_method_options;

    @SerializedName("raw")
    private Error error;

    public Error getError() {
        return error;
    }

    public double getTransactionAmount() {
        if (amount_received <= 0) return 0;
        return amount_received / 100f;
    }

    public String getReceiptUrl() {
        if (charges == null) return "";
        List<Data> datas = charges.getData();
        if (datas == null || datas.size() <= 0) return "";
        return datas.get(0).getReceipt_url();
    }

    public int getInvoice_number() {
        return invoice_number;
    }

    public String getStatus() {
        return status;
    }

    public List<String> getPayment_method_types() {
        return payment_method_types;
    }

    public Payment_method_options getPayment_method_options() {
        return payment_method_options;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public boolean getLivemode() {
        return livemode;
    }

    public String getCurrency() {
        return currency;
    }

    public int getCreated() {
        return created;
    }

    public String getConfirmation_method() {
        return confirmation_method;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public Charges getCharges() {
        return charges;
    }

    public String getCapture_method() {
        return capture_method;
    }

    public String getApplication() {
        return application;
    }

    public long getAmount_received() {
        return amount_received;
    }

    public long getAmount_capturable() {
        return amount_capturable;
    }

    public long getAmount() {
        return amount;
    }

    public String getObject() {
        return object;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        if (description == null) return "";
        return description;
    }

    public static class Payment_method_options {
        private Card card;

        public Card getCard() {
            return card;
        }
    }

    public static class Card {
        private String request_three_d_secure;

        public String getRequest_three_d_secure() {
            return request_three_d_secure;
        }
    }

    public static class Charges {
        private String url;
        private int total_count;
        private boolean has_more;
        private List<Data> data;
        private String object;

        public String getUrl() {
            return url;
        }

        public int getTotal_count() {
            return total_count;
        }

        public boolean getHas_more() {
            return has_more;
        }

        public List<Data> getData() {
            return data;
        }

        public String getObject() {
            return object;
        }
    }

    public static class Data {
        private String status;
        private Refunds refunds;
        private boolean refunded;
        private String receipt_url;
        private Payment_method_details payment_method_details;
        private String payment_method;
        private String payment_intent;
        private boolean paid;
        private Outcome outcome;
        private boolean livemode;
        private boolean disputed;
        private String currency;
        private int created;
        private boolean captured;
        private String calculated_statement_descriptor;
        private Billing_details billing_details;
        private String balance_transaction;
        private String application;
        private int amount_refunded;
        private int amount;
        private String object;
        private String id;

        public String getStatus() {
            return status;
        }

        public Refunds getRefunds() {
            return refunds;
        }

        public boolean getRefunded() {
            return refunded;
        }

        public String getReceipt_url() {
            return receipt_url;
        }

        public Payment_method_details getPayment_method_details() {
            return payment_method_details;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public String getPayment_intent() {
            return payment_intent;
        }

        public boolean getPaid() {
            return paid;
        }

        public Outcome getOutcome() {
            return outcome;
        }

        public boolean getLivemode() {
            return livemode;
        }

        public boolean getDisputed() {
            return disputed;
        }

        public String getCurrency() {
            return currency;
        }

        public int getCreated() {
            return created;
        }

        public boolean getCaptured() {
            return captured;
        }

        public String getCalculated_statement_descriptor() {
            return calculated_statement_descriptor;
        }

        public Billing_details getBilling_details() {
            return billing_details;
        }

        public String getBalance_transaction() {
            return balance_transaction;
        }

        public String getApplication() {
            return application;
        }

        public int getAmount_refunded() {
            return amount_refunded;
        }

        public int getAmount() {
            return amount;
        }

        public String getObject() {
            return object;
        }

        public String getId() {
            return id;
        }
    }

    public static class Refunds {
        private String url;
        private int total_count;
        private boolean has_more;
        private List<Data> data;
        private String object;

        public String getUrl() {
            return url;
        }

        public int getTotal_count() {
            return total_count;
        }

        public boolean getHas_more() {
            return has_more;
        }

        public List<Data> getData() {
            return data;
        }

        public String getObject() {
            return object;
        }
    }


    public static class Payment_method_details {
        private String type;
        private Card card;

        public String getType() {
            return type;
        }

        public Card getCard() {
            return card;
        }
    }

    public static class Three_d_secure {
        private String version;
        private boolean succeeded;
        private boolean authenticated;

        public String getVersion() {
            return version;
        }

        public boolean getSucceeded() {
            return succeeded;
        }

        public boolean getAuthenticated() {
            return authenticated;
        }
    }

    public static class Checks {
        private String address_postal_code_check;

        public String getAddress_postal_code_check() {
            return address_postal_code_check;
        }
    }

    public static class Outcome {
        private String type;
        private String seller_message;
        private int risk_score;
        private String risk_level;
        private String network_status;

        public String getType() {
            return type;
        }

        public String getSeller_message() {
            return seller_message;
        }

        public int getRisk_score() {
            return risk_score;
        }

        public String getRisk_level() {
            return risk_level;
        }

        public String getNetwork_status() {
            return network_status;
        }
    }

    public static class Billing_details {
        private Address address;

        public Address getAddress() {
            return address;
        }
    }

    public static class Address {
        private String postal_code;

        public String getPostal_code() {
            return postal_code;
        }
    }
}
