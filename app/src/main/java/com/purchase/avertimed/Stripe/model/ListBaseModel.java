package com.purchase.avertimed.Stripe.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListBaseModel<T> extends BaseModel {

    @SerializedName(value = "data", alternate = "userslists")
    private List<T> data;
    private int page;
    private int limit;
    private int total;
    private Count count;

    public ListBaseModel(List<T> data) {
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public T getFirstItem() {
        if (data == null || data.size() <= 0) return null;
        return data.get(0);
    }

    public int getPage() {
        return page;
    }

    public boolean isFirstPage() {
        return page == 0;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCurrent() {
        return limit * (page + 1);
    }

    public boolean hasMore() {
        return getCurrent() < total;
    }

    public int itemCount() {
        if (data == null) return 0;
        return data.size();
    }

    public Count getCount() {
        return count;
    }
}
